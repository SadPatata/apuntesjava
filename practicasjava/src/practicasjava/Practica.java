package practicasjava;

import java.util.Scanner;

//Autor: Iker RU
//fecha: 30/10/20
//Programa: Calculadora


public class Practica {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);

//Variables		
		double a;
		double b;
		double res;
		double prim;
		double sec;
		int pisos;
		int opcion;
		boolean exit = false;
		
		
//Menu inicial		
		
		System.out.println("    CALCULADORA     ");
		System.out.println(" ___________________");
		System.out.println("|Numeros a tratar:  |");
		System.out.println("|Numero 1:          |");
		System.out.println("|___________________|");
		System.out.println(" ");
		a =  sc.nextDouble();
		System.out.println(" ___________________");
		System.out.println("|Numero 2:          |");
		System.out.println("|___________________|");
		System.out.println(" ");
		b =  sc.nextDouble();

		
		
//Bucle de calculadora	
		do 
		{	System.out.println(" ___________________");
			System.out.println("|Sumar: 1           |");
			System.out.println("|Restar: 2          |");
			System.out.println("|Multiplicar: 3     |");
			System.out.println("|Dividir: 4         |");
			System.out.println("|Residuo: 5         |");
			System.out.println("|Potencia: 6        |");
			System.out.println("|Raiz Cuadrada 7    |");
			System.out.println("|Fibonacci 8        |");
			System.out.println("|Nuevos numeros: 9  |");
			System.out.println("|Salir: 0           |");
			System.out.println("|___________________|");
			System.out.println("Numeros: " + a + ", " + b);
			System.out.println(" ");
			opcion = sc.nextInt();
			
			switch(opcion)
			{
				case 1:
						res = a + b;
						System.out.println("La suma es: " + res);
						break;
					
				case 2:
						res = a - b;
						System.out.println("La resta es: " + res);
						break;
				case 3:
						res = a * b;
						System.out.println("La multiplicacion es: " + res);
						break;
				case 4:
						res = a / b;
						System.out.println("La division es: " + res);
						break;
				case 5:
						res = a % b;
						System.out.println("La residuo es: " + res);
						break;						
				case 6:					
					
						res = a;
						for(int i = 0; i < b-1; i++)
						{
							res = res*a;						
						}				
						System.out.println("La potencia es: " + res);
						break;
				case 7:
			
						res = Math.sqrt(a);
						System.out.println("La raiz de " + a + " es: " + res);
						res = Math.sqrt(b);
						System.out.println("La raiz de " + b + " es: " + res);
						break;
				case 8:
						System.out.println("Numeros a mostrar: ");
						System.out.println(" ");
						pisos = sc.nextInt();
						res = 0;
						prim = 0;
						sec = 1;
						System.out.println(" ");
						System.out.println("Fibonacci: 0.0");
						System.out.println(" ");
						System.out.println("Fibonacci: 1.0");
						for(int j = 2; j < pisos; j++)
						{
							res = prim + sec; 
							prim = sec;
							sec = res; 
				
							System.out.println(" ");
							System.out.println("Fibonacci: " + res);
						}
					

						
						break;
				case 9:					
						System.out.println("Numero 1: ");
						a =  sc.nextDouble();
						System.out.println("Numero 2: ");
						b =  sc.nextDouble();
					break;			
					
				case 0:
				
						exit = true;
				
						break;
				default:		
						 System.out.println("Invalid input try again");
						break;
			
			}
			
			System.out.println(" ");
			
			
		}while(exit == false);
		
	}

}